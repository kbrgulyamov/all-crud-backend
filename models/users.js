const mongoose = require('mongoose')

var Users = mongoose.Schema({
    name: {
        type: String,
        required: false,
        default: "users not name"
    },
    password: {
        type: String,
        required: true
    },
    url: {
        type: String,
        default: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTVntqrcr4IRDBqiezuyhbD2AWHShx9YGEAYg&usqp=CAU"
    },
    products: {
        type: String
    },
    productsCount: {
        type: Number,
        default: 0
    }
})


module.exports = mongoose.model('Users', Users)
